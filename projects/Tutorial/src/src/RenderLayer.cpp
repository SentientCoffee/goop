#include "RenderLayer.h"

#include "CameraComponent.h"

#include <florp/app/Timing.h>
#include <florp/game/RenderableComponent.h>
#include <florp/game/SceneManager.h>
#include <florp/game/Transform.h>
#include "FrameState.h"

using namespace florp::app;
using namespace florp::game;
using namespace florp::graphics;

void sortRenderers(entt::registry& reg) {
	// We sort our mesh renderers based on material properties
	// This will group all of our meshes based on shader first, then material second
	reg.sort<RenderableComponent>([](const RenderableComponent& lhs, const RenderableComponent& rhs) {
		if(rhs.Material == nullptr || rhs.Mesh == nullptr) return false;
		if(lhs.Material == nullptr || lhs.Mesh == nullptr) return true;
		if(lhs.Material->RasterState.Blending.BlendEnabled & !rhs.Material->RasterState.Blending.BlendEnabled) return false;
		if(!lhs.Material->RasterState.Blending.BlendEnabled & rhs.Material->RasterState.Blending.BlendEnabled) return true;
		if(lhs.Material->GetShader() != rhs.Material->GetShader()) return lhs.Material->GetShader() < rhs.Material->GetShader();
		return lhs.Material < rhs.Material;
	});
}

void ctorSort(entt::entity, entt::registry& ecs, const RenderableComponent& r) {
	sortRenderers(ecs);
}

void dtorSort(entt::entity, entt::registry& ecs) {
	sortRenderers(ecs);
}

void RenderLayer::OnWindowResize(uint32_t width, uint32_t height) {
	// ReSharper disable once CppMissingIndent
	CurrentRegistry().view<CameraComponent>().each([&](auto entity, CameraComponent& cam) {
		if(cam.isMainCamera) {
			cam.backBuffer->resize(width, height);
			if(cam.frontBuffer != nullptr) {
				cam.frontBuffer->resize(width, height);
			}
		}
	});
}

void RenderLayer::OnSceneEnter() {
	CurrentRegistry().on_construct<RenderableComponent>().connect<&ctorSort>();
	CurrentRegistry().on_destroy<RenderableComponent>().connect<&dtorSort>();
}

void RenderLayer::Render() {
	auto& ecs = CurrentRegistry();

	Material::Sptr material = nullptr;
	Shader::Sptr boundShader = nullptr;

	ecs.sort<CameraComponent>([](const CameraComponent& first, const CameraComponent& second) {
		return second.isMainCamera;
	});

	ecs.view<CameraComponent>().each([&](auto ent, CameraComponent& camComponent) {
		const Transform camTransform = ecs.get<Transform>(ent);

		const glm::vec3 position = camTransform.GetLocalPosition();
		const glm::mat4 viewMatrix = glm::inverse(camTransform.GetWorldTransform());
		const glm::mat4 viewProjection = camComponent.projection * viewMatrix;
		
		if(camComponent.isMainCamera) {
			auto& afs = ecs.ctx_or_set<AppFrameState>();
			afs.last = afs.current;
			afs.last.output = camComponent.frontBuffer != nullptr ? camComponent.backBuffer : nullptr;
			afs.current.output = camComponent.frontBuffer != nullptr ? camComponent.frontBuffer : camComponent.backBuffer;
			afs.current.viewMatrix = viewMatrix;
			afs.current.projectionMatrix = camComponent.projection;
			afs.current.viewProjection = viewProjection;
		}

		camComponent.backBuffer->bind();
		glViewport(0, 0, camComponent.backBuffer->getWidth(), camComponent.backBuffer->getHeight());
		glClearColor(camComponent.clearColor.x, camComponent.clearColor.y, camComponent.clearColor.z, camComponent.clearColor.z);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);

		// A view will let us iterate over all of our entities that have the given component types
		auto view = ecs.view<RenderableComponent>();

		for(const auto& entity : view) {

			// Get our shader
			const RenderableComponent& renderer = ecs.get<RenderableComponent>(entity);

			// Early bail if mesh is invalid
			if(renderer.Mesh == nullptr || renderer.Material == nullptr)
				continue;

			// If our shader has changed, we need to bind it and update our frame-level uniforms
			if(renderer.Material->GetShader() != boundShader) {
				boundShader = renderer.Material->GetShader();
				boundShader->Use();
				boundShader->SetUniform("uCameraPos", position);
				//boundShader->SetUniform("uTime", florp::app::Timing::GameTime);
			}

			// If our material has changed, we need to apply it to the shader
			if(renderer.Material != material) {
				material = renderer.Material;
				
				material->Set("uLights[0].position", { 5 * glm::sin(10 * Timing::GameTime), 2, 2 * glm::cos(10 * Timing::GameTime + glm::radians(45.0f)) });
				material->Set("uLights[0].color", { 1.0f, 0.0f, 1.0f });
				material->Set("uLights[0].shininess", 32.0f);
				material->Set("uLights[0].attenuation", 0.5f);

				material->Set("uLights[1].position", { 2 * glm::cos(10 * Timing::GameTime + glm::radians(45.0f)), 0, 10 * glm::sin(5 * Timing::GameTime) });
				material->Set("uLights[1].color", { 0.0f, 1.0f, 0.0f });
				material->Set("uLights[1].shininess", 256.0f);
				material->Set("uLights[1].attenuation", 0.3f);

				material->Set("uNumLights", 2);
				
				material->Apply();
			}

			// We'll need some info about the entities position in the world
			const Transform& transform = ecs.get_or_assign<Transform>(entity);

			// Our normal matrix is the inverse-transpose of our object's world rotation
			glm::mat3 normalMatrix = glm::mat3(glm::transpose(glm::inverse(transform.GetWorldTransform())));

			// Update the MVP using the item's transform
			boundShader->SetUniform("uViewProjection", viewProjection);

			// Update the model matrix to the item's world transform
			boundShader->SetUniform("uModel", transform.GetWorldTransform());

			// Update the model matrix to the item's world transform
			boundShader->SetUniform("uNormalMatrix", normalMatrix);

			// Draw the item
			renderer.Mesh->Draw();

		}
		
		camComponent.backBuffer->unbind();
		if(camComponent.frontBuffer != nullptr) {
			const auto temp = camComponent.backBuffer;
			camComponent.backBuffer = camComponent.frontBuffer;
			camComponent.frontBuffer = temp;
		}
	});
}