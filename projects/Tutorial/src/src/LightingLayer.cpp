#include "LightingLayer.h"
#include "ShadowLight.h"

#include <florp/app/Application.h>
#include <florp/game/RenderableComponent.h>
#include <florp/game/SceneManager.h>
#include <florp/game/Transform.h>

#include "FrameState.h"

using namespace florp::app;
using namespace florp::game;
using namespace florp::graphics;

void LightingLayer::OnWindowResize(uint32_t width, uint32_t height) {
	_accumulationBuffer->resize(width, height);
}

void LightingLayer::Initialize() {
	Application* app = Application::Get();

	_shadowMappingShader = std::make_shared<Shader>();
	_shadowMappingShader->LoadPart(ShaderStageType::VertexShader, "shaders/simple.vs.glsl");
	_shadowMappingShader->Link();

	_maskedShader = std::make_shared<Shader>();
	_maskedShader->LoadPart(ShaderStageType::VertexShader, "shaders/simple.vs.glsl");  
	_maskedShader->LoadPart(ShaderStageType::FragmentShader, "shaders/shadow_masked.fs.glsl");
	_maskedShader->Link();

	_shadowCompositeShader = std::make_shared<Shader>();
	_shadowCompositeShader->LoadPart(ShaderStageType::VertexShader, "shaders/post-processing/post-process.vs.glsl");
	_shadowCompositeShader->LoadPart(ShaderStageType::FragmentShader, "shaders/post-processing/shadow_post.fs.glsl");
	_shadowCompositeShader->Link();

	_finalCompositeShader = std::make_shared<Shader>();
	_finalCompositeShader->LoadPart(ShaderStageType::VertexShader, "shaders/post-processing/post-process.vs.glsl");
	_finalCompositeShader->LoadPart(ShaderStageType::FragmentShader, "shaders/post-processing/lighting_composite.fs.glsl");
	_finalCompositeShader->Link();
	_finalCompositeShader->SetUniform("uExposure", 1.0f);

	RenderBufferDescription mainColor = RenderBufferDescription();
	mainColor.isShaderReadable = true;
	mainColor.attachment = RenderTargetAttachment::Color0;
	mainColor.targetType = RenderTargetType::RGB16F;

	_accumulationBuffer = std::make_shared<Framebuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
	_accumulationBuffer->SetDebugName("Intermediate buffer");
	_accumulationBuffer->addAttachment(mainColor);
	_accumulationBuffer->validateBuffer();

	{
		float vert[] = {
			-1.0f, -1.0f, 0.0f, 0.0f,
			1.0f, -1.0f, 1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f
		};
		uint32_t indices[] = {
			0, 1, 2,
			1, 3, 2
		};
		BufferLayout layout = {
			{ "inPosition", ShaderDataType::Float2 },
			{ "inUV",       ShaderDataType::Float2 }
		};

		_fullscreenQuad = std::make_shared<Mesh>(vert, 4, layout, indices, 6);
	}
	
}
void LightingLayer::PreRender() {
	auto& ecs = CurrentRegistry();

	const auto view = ecs.view<ShadowLight>();
	if(!view.empty()) {
		glEnable(GL_DEPTH_TEST);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		Shader::Sptr shader = nullptr;
		ecs.view<ShadowLight>().each([&](auto entity, ShadowLight& light) {
			const Transform& lightTransform = ecs.get<Transform>(entity);

			if(light.mask == nullptr) {
				shader = _shadowMappingShader;
			}
			else {
				shader = _maskedShader;
				light.mask->Bind(0);
			}

			shader->Use();
			shader->SetUniform("uOutputResolution", light.shadowBuffer->getSize());

			light.shadowBuffer->bind();
			glViewport(0, 0, light.shadowBuffer->getWidth(), light.shadowBuffer->getHeight());
			glClear(GL_DEPTH_BUFFER_BIT);

			//glm::vec3 position = lightTransform.GetLocalPosition();
			const glm::mat4 viewMat = glm::inverse(lightTransform.GetWorldTransform());
			const glm::mat4 viewProjection = light.projection * viewMat;

			auto insideView = ecs.view<RenderableComponent>();
			for(const auto& ent : insideView) {
				const RenderableComponent& renderable = ecs.get<RenderableComponent>(ent);
				if(renderable.Mesh == nullptr || renderable.Material == nullptr || !renderable.Material->IsShadowCaster) {
					continue;
				}

				const Transform& transform = ecs.get_or_assign<Transform>(ent);
				shader->SetUniform("uModelViewProjection", viewProjection * transform.GetWorldTransform());

				renderable.Mesh->Draw();
			}
			
			light.shadowBuffer->unbind();
		});

		glCullFace(GL_BACK);
	}
	
}

void LightingLayer::PostRender() {
	_accumulationBuffer->bind();
	glClearColor(_ambientLight.r, _ambientLight.g, _ambientLight.b, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_ONE, GL_ONE);

	postProcessShadows();
	postProcessLights();

	_accumulationBuffer->unbind();

	glDisable(GL_BLEND);

	Application* app = Application::Get();
	auto& ecs = CurrentRegistry();

	const AppFrameState& state = ecs.ctx<AppFrameState>();
	Framebuffer::sptr mainBuffer = state.current.output;

	mainBuffer->bind();
	_finalCompositeShader->Use();
	mainBuffer->Bind(1, RenderTargetAttachment::Color0);
	_accumulationBuffer->Bind(2);

	_fullscreenQuad->Draw();
	mainBuffer->unbind();
}

void LightingLayer::RenderGUI() {}

void LightingLayer::postProcessLights() {}

void LightingLayer::postProcessShadows() {
	Application* app = Application::Get();
	auto& ecs = CurrentRegistry();
	
	const AppFrameState& state = ecs.ctx<AppFrameState>();
	Framebuffer::sptr mainBuffer = state.current.output;

	const float pm22 = state.current.projectionMatrix[2][2];
	const float pm32 = state.current.projectionMatrix[3][2];
	const float nearPlane = 2.0f * pm32 / (2.0f * pm22 - 2.0f);
	const float farPlane = (pm22 - 1.0f) * nearPlane / (pm22 + 1.0f);
	
	const glm::mat4 viewInv = glm::inverse(state.current.viewMatrix);
	const glm::mat4 projInv = glm::inverse(state.current.projectionMatrix);
	const glm::mat4 vpInv = glm::inverse(state.current.viewProjection);
	const glm::vec3 cameraPos = glm::vec3(viewInv * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

	_shadowCompositeShader->Use();
	_shadowCompositeShader->SetUniform("uView", state.current.viewMatrix);
	_shadowCompositeShader->SetUniform("uViewInverse", viewInv);
	_shadowCompositeShader->SetUniform("uCameraPos", cameraPos);
	_shadowCompositeShader->SetUniform("uProjectionInverse", projInv);
	_shadowCompositeShader->SetUniform("uViewProjectionInverse", vpInv);
	_shadowCompositeShader->SetUniform("uNearPlane", nearPlane);
	_shadowCompositeShader->SetUniform("uFarPlane", farPlane);
	_shadowCompositeShader->SetUniform("uBias", 0.000001f);
	_shadowCompositeShader->SetUniform("uMaterialShininess", 1.0f); // TODO: This should be values from the GBuffer

	mainBuffer->Bind(0, RenderTargetAttachment::Color0);
	mainBuffer->Bind(1, RenderTargetAttachment::Depth);
	mainBuffer->Bind(3, RenderTargetAttachment::Color1);

	const auto view = CurrentRegistry().view<ShadowLight>();
	if(!view.empty()) {
		view.each([&](auto entity, ShadowLight& light) {
			const Transform& transform = ecs.get_or_assign<Transform>(entity);
			glm::vec3 pos = glm::vec3(transform.GetWorldTransform() * glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));

			if(light.projectorImage != nullptr) {
				_shadowCompositeShader->SetUniform("uIsProjector", true);
				_shadowCompositeShader->SetUniform("uProjectorIntensity", light.projectorImageIntensity);
				light.projectorImage->Bind(4);
			}
			else {
				_shadowCompositeShader->SetUniform("uIsProjector", false);
			}

			_shadowCompositeShader->SetUniform("uLightView", light.projection * glm::inverse(transform.GetWorldTransform()));
			_shadowCompositeShader->SetUniform("uLightPos", pos);
			_shadowCompositeShader->SetUniform("uLightDirection", glm::mat3(transform.GetWorldTransform()) * glm::vec3(0.0f, 0.0f, -1.0f));
			_shadowCompositeShader->SetUniform("uLightColor", light.color);
			_shadowCompositeShader->SetUniform("uLightAttenuation", light.attenuation);

			light.shadowBuffer->Bind(2, RenderTargetAttachment::Depth);
			_fullscreenQuad->Draw();
		});
	}
}
