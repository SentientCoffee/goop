#include "SceneBuilder.h"

#include "CameraComponent.h"
#include "ControlBehaviour.h"
#include "RotateBehaviour.h"
#include "ShadowLight.h"

#include <florp/app/Application.h>
#include <florp/game/RenderableComponent.h>
#include <florp/game/SceneManager.h>
#include <florp/game/Transform.h>
#include <florp/graphics/MeshBuilder.h>
#include <florp/graphics/MeshData.h>
#include <florp/graphics/ObjLoader.h>

#include <entt.hpp>
#include <GLM/gtc/matrix_transform.hpp>

using namespace florp::app;
using namespace florp::game;
using namespace florp::graphics;

static ShadowLight& createShadowCaster(Scene* scene, entt::entity* outEntity, const glm::vec3& pos, const glm::vec3& target, const glm::vec3& up,
	const float distance = 10.0f, const float fov = 60.0f, const glm::ivec2& bufferSize = { 1024, 1024 }, const std::string& name = "") {
	RenderBufferDescription depth = RenderBufferDescription();
	depth.isShaderReadable = true;
	depth.attachment = RenderTargetAttachment::Depth;
	depth.targetType = RenderTargetType::Depth32;

	Framebuffer::sptr shadowBuffer = std::make_shared<Framebuffer>(bufferSize.x, bufferSize.y);
	if(!name.empty()) { shadowBuffer->SetDebugName(name); }
	shadowBuffer->addAttachment(depth);
	shadowBuffer->validateBuffer();

	entt::entity e = scene->CreateEntity();
	ShadowLight& light = scene->Registry().assign<ShadowLight>(e);
	light.shadowBuffer = shadowBuffer;
	light.projection = glm::perspective(glm::radians(fov), static_cast<float>(bufferSize.x) / static_cast<float>(bufferSize.y), 0.25f, distance);
	light.attenuation = 1.0f / distance;
	light.color = glm::vec3(1.0f);

	Transform& t = scene->Registry().get<Transform>(e);
	t.SetPosition(pos);
	t.LookAt(target, up);

	if(outEntity != nullptr) { *outEntity = e; }

	return light;
}

void SceneBuilder::Initialize() {

	Application* app = Application::Get();

	auto* scene = SceneManager::RegisterScene("main");
	SceneManager::SetCurrentScene("main");

	MeshData data = ObjLoader::LoadObj("meshes/monkey.obj", glm::vec4(1.0f));

	Shader::Sptr shader = std::make_shared<Shader>();
	shader->LoadPart(ShaderStageType::VertexShader, "shaders/lighting.vs.glsl");
	shader->LoadPart(ShaderStageType::FragmentShader, "shaders/blinn-phong-multi.fs.glsl");
	shader->Link();

	Material::Sptr mat = std::make_shared<Material>(shader);
	mat->Set("uAmbientColor", { 1.0f, 1.0f, 1.0f });
	mat->Set("uAmbientPower", 0.2f);
	mat->Set("uMaterialShininess", 256.0f);

	mat->Set("uLights[0].position", { 2, 0, 0 });
	mat->Set("uLights[0].color", glm::vec3(1.0f, 0.0f, 1.0f) * 0.3f);
	mat->Set("uLights[0].attenuation", 1.0f / 10.0f);
	mat->Set("uLights[1].position", { -2, 0, 0 });
	mat->Set("uLights[1].color", glm::vec3(0.0f, 1.0f, 0.0f) * 0.3f);
	mat->Set("uLights[1].attenuation", 1.0f / 10.0f);

	mat->Set("uNumLights", 2);
	mat->Set("uAlbedo", Texture2D::LoadFromFile("textures/marble.png", false, true, true));

	const int numMonkeys = 6;
	const float step = glm::two_pi<float>() / numMonkeys;

	// Spinning monkey head
	for(int i = 0; i < numMonkeys; ++i) {
		const entt::entity test = scene->CreateEntity();

		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(test);
		renderable.Mesh = MeshBuilder::Bake(data);
		renderable.Material = mat;

		Transform& t = scene->Registry().get<Transform>(test);
		t.SetPosition({ glm::cos(step * static_cast<float>(i)) * 5.0f, 0.0f, glm::sin(step * static_cast<float>(i)) * 5.0f });
		t.SetEulerAngles({ -90.0f, glm::degrees(-step * static_cast<float>(i)), 0.0f });

		scene->AddBehaviour<AxialSpinBehaviour>(test, glm::vec3(0.0f), glm::vec3(0, 1, 0), 45.0f);
		scene->AddBehaviour<RotateBehaviour>(test, glm::vec3(0.0f, 0.0f, 45.0f));
	}

	{
		entt::entity test = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(test);
		renderable.Mesh = MeshBuilder::Bake(data);
		renderable.Material = mat;
		//Transform& t = scene->Registry().get<Transform>(test);
		scene->AddBehaviour<RotateBehaviour>(test, glm::vec3(45.0f, 45.0f, 45.0f));
	}

	MeshData indicatorCube = MeshBuilder::Begin();
	MeshBuilder::AddAlignedCube(indicatorCube, glm::vec3(0.0f), glm::vec3(0.1f));
	Mesh::Sptr indicatorMesh = MeshBuilder::Bake(indicatorCube);

	// Main camera component
	{
		// The color buffer should be marked as shader readable, so that we generate a texture for it
		RenderBufferDescription mainColor = RenderBufferDescription();
		mainColor.isShaderReadable = true;
		mainColor.attachment = RenderTargetAttachment::Color0;
		mainColor.targetType = RenderTargetType::RGB8;

		RenderBufferDescription normal = RenderBufferDescription();
		normal.isShaderReadable = true;
		normal.attachment = RenderTargetAttachment::Color1;
		normal.targetType = RenderTargetType::RGB10;


		// The depth attachment does not need to be a texture (and would cause issues since the format is DepthStencil)
		RenderBufferDescription depth = RenderBufferDescription();
		depth.isShaderReadable = true;
		depth.attachment = RenderTargetAttachment::Depth;
		depth.targetType = RenderTargetType::Depth32;

		// Our main frame buffer needs a color output, and a depth output
		Framebuffer::sptr buffer = std::make_shared<Framebuffer>(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight());
		buffer->SetDebugName("MainBuffer");
		buffer->addAttachment(mainColor);
		buffer->addAttachment(normal);
		buffer->addAttachment(depth);
		buffer->validateBuffer();

		const entt::entity camera = scene->CreateEntity();
		CameraComponent& cam = scene->Registry().assign<CameraComponent>(camera);
		cam.backBuffer = buffer;
		cam.frontBuffer = buffer->clone();
		cam.isMainCamera = true;
		cam.projection = glm::perspective(glm::radians(60.0f), 1.0f, 0.01f, 1000.0f);

		Transform& t = scene->Registry().get<Transform>(camera);
		t.SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
		t.LookAt(glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

		scene->AddBehaviour<ControlBehaviour>(camera, glm::vec3(0.0f, 0.0f, 0.0f));

		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(camera);
		renderable.Mesh = indicatorMesh;
		renderable.Material = mat;
	}

	auto& light = createShadowCaster(scene, nullptr, { 0.0f, 5.0f, 0.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f }, 25.0f);
	light.color = { 1.0f, 1.0f, 1.0f };
	light.attenuation = 1.0f / 15.0f;
	light.projectorImage = Texture2D::LoadFromFile("textures/light_projection.png", false, false, true);

	for(int i = 0; i < numMonkeys; ++i) {
		auto& lgh = createShadowCaster(scene, nullptr, { glm::cos(step * static_cast<float>(i)) * 9.0f, 3.5f, glm::sin(step * static_cast<float>(i)) * 9.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, 25.0f, 75.0f);
		lgh.color = glm::vec3(glm::sin(static_cast<float>(i) * step) + 1.0f, glm::cos(static_cast<float>(i) * step) + 1.0f, glm::sin((static_cast<float>(i) * step) + glm::pi<float>()) + 1.0f) / 2.0f * 0.9f;
		lgh.attenuation = 1.0f / 20.0f;

		const entt::entity entity = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(entity);
		renderable.Mesh = indicatorMesh;
		renderable.Material = mat;
		Transform& t = scene->Registry().get<Transform>(entity);
		t.SetPosition(glm::vec3(glm::cos(step * static_cast<float>(i)) * 9.0f, 2.0f, glm::sin(step * static_cast<float>(i)) * 9.0f));
	}

	{
		MeshData d = MeshBuilder::Begin();
		MeshBuilder::AddAlignedCube(d, { 0.0f, -1.0f, 0.0 }, { 100.0f, 0.1f, 100.0f });
		Mesh::Sptr mesh = MeshBuilder::Bake(d);

		entt::entity entity = scene->CreateEntity();
		RenderableComponent& renderable = scene->Registry().assign<RenderableComponent>(entity);
		renderable.Mesh = MeshBuilder::Bake(d);
		renderable.Material = mat;
	}
}
