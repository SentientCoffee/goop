#include "PostProcessingLayer.h"

#include <florp/app/Application.h>
#include <florp/game/SceneManager.h>
#include <imgui.h>
#include <GLM/gtc/type_ptr.hpp>
#include "FrameState.h"

using namespace florp::app;
using namespace florp::game;
using namespace florp::graphics;

PostProcessingLayer::PostProcessingLayer() {
	{
		float vert[] = {
			-1.0f, -1.0f, 0.0f, 0.0f,
			1.0f, -1.0f, 1.0f, 0.0f,
			-1.0f, 1.0f, 0.0f, 1.0f,
			1.0f, 1.0f, 1.0f, 1.0f
		};
		uint32_t indices[] = {
			0, 1, 2,
			1, 3, 2
		};
		BufferLayout layout = {
			{ "inPosition", ShaderDataType::Float2 },
			{ "inUV",       ShaderDataType::Float2 }
		};

		_fullscreenQuad = std::make_shared<Mesh>(vert, 4, layout, indices, 6);
	}
	
	// Bloom
	_passes["highlight"] = createPass("Highlight", "shaders/post-processing/bloom_highlight.fs.glsl");

	for(unsigned i = 1; i <= 10; ++i) {
		_passes["hBlur" + std::to_string(i)] = createPass("Horizontal blur " + std::to_string(i), "shaders/post-processing/bloom_blur.fs.glsl");
		_passes["vBlur" + std::to_string(i)] = createPass("Vertical blur " + std::to_string(i), "shaders/post-processing/bloom_blur.fs.glsl");
	}

	_passes["additiveBlend"] = createPass("Additive Blend", "shaders/post-processing/bloom_additive_blend.fs.glsl");
	_passes["additiveBlend"]->inputs.push_back({ nullptr });
	_passes["additiveBlend"]->inputs.push_back({ _passes["vBlur10"] });

	// Motion blur
	_passes["motionBlur"] = createPass("Motion blur", "shaders/post-processing/motion_blur.fs.glsl");
	_passes["motionBlur"]->inputs.push_back({ nullptr, RenderTargetAttachment::Depth });
	
	// Post-processing effects
	_passes["checkerboard"] = createPass("Checkerboard", "shaders/post-processing/checker.fs.glsl");
	_passes["inversion"] = createPass("Inversion", "shaders/post-processing/inversion.fs.glsl");
	_passes["grayscale"] = createPass("Grayscale", "shaders/post-processing/grayscale.fs.glsl");
	_passes["kernel"] = createPass("Kernel", "shaders/post-processing/kernel.fs.glsl");
	_passes["gamma"] = createPass("Gamma", "shaders/post-processing/gamma.fs.glsl");
}

PostProcessingLayer::PostPass::sptr PostProcessingLayer::createPass(const std::string& name, const std::string_view fragmentPath, const float scale) const {
	Application* app = Application::Get();

	RenderBufferDescription mainColor;  // NOLINT(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
	mainColor.isShaderReadable = true;
	mainColor.attachment = RenderTargetAttachment::Color0;
	mainColor.targetType = RenderTargetType::RGB8;
	
	auto shader = std::make_shared<Shader>();
	shader->SetDebugName(name + " shader");
	shader->LoadPart(ShaderStageType::VertexShader, "shaders/post-processing/post-process.vs.glsl");
	shader->LoadPart(ShaderStageType::FragmentShader, fragmentPath.data());
	shader->Link();

	auto output = std::make_shared<Framebuffer>(static_cast<uint32_t>(app->GetWindow()->GetWidth() * scale), static_cast<uint32_t>(app->GetWindow()->GetHeight() * scale));
	output->SetDebugName(name + " framebuffer");
	output->addAttachment(mainColor);
	output->validateBuffer();

	auto result = std::make_shared<PostPass>();
	result->shader = shader;
	result->output = output;

	return result;
	
}

void PostProcessingLayer::OnWindowResize(const uint32_t width, const uint32_t height) {
	for(auto& pass : _passes) {
		pass.second->output->resize(static_cast<uint32_t>(width * pass.second->resolutionMultiplier),
			static_cast<uint32_t>(height * pass.second->resolutionMultiplier));
	}
}

void PostProcessingLayer::PostRender() {
	Application* app = Application::Get();

	const auto& afs = CurrentRegistry().ctx<AppFrameState>();
	const Framebuffer::sptr mainBuffer = afs.current.output;
	
	glDisable(GL_DEPTH_TEST);
	
	// The last output will start as the output from the rendering
	Framebuffer::sptr lastPass = mainBuffer;
	// We'll iterate over all of our render passes
	for(const auto& pass : _passes) {
		if(!pass.second->active) continue;
		
		// We'll bind our post-processing output as the current render target and clear it
		pass.second->output->bind(RenderTargetBinding::Draw);
		glClear(GL_COLOR_BUFFER_BIT);
		// Set the viewport to be the entire size of the passes output
		glViewport(0, 0, pass.second->output->getWidth(), pass.second->output->getHeight());
		// Use the post processing shader to draw the fullscreen quad
		pass.second->shader->Use();
		pass.second->shader->SetUniform("uView", afs.current.viewMatrix);
		pass.second->shader->SetUniform("uProjection", afs.current.projectionMatrix);
		pass.second->shader->SetUniform("uViewProjection", afs.current.viewProjection);
		pass.second->shader->SetUniform("uViewProjectionInverse", glm::inverse(afs.current.viewProjection));
		
		pass.second->shader->SetUniform("uLastView", afs.last.viewMatrix);
		pass.second->shader->SetUniform("uLastProjection", afs.last.projectionMatrix);
		pass.second->shader->SetUniform("uLastViewProjection", afs.last.viewProjection);
		pass.second->shader->SetUniform("uLastViewProjectionInverse", glm::inverse(afs.last.viewProjection));
		
		lastPass->getAttachment(RenderTargetAttachment::Color0)->Bind(0);
		pass.second->shader->SetUniform("xImage", 0);
		pass.second->shader->SetUniform("xScreenRes", glm::ivec2(app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight()));

		for(unsigned i = 0; i < pass.second->inputs.size(); ++i) {
			const auto& input = pass.second->inputs[i];
			
			if(input.pass == nullptr) {
				if(input.usePreviousFrame && afs.last.output != nullptr) {
					afs.last.output->Bind(i + 1, input.attachment);
				}
				else {
					mainBuffer->Bind(i + 1, input.attachment);
				}
			}
			else {
				input.pass->output->Bind(i + 1, input.attachment);
			}
		}

		_fullscreenQuad->Draw();
		// Unbind the output pass so that we can read from it
		pass.second->output->unbind();
		// Update the last pass output to be this passes output
		lastPass = pass.second->output;
	}
	
	// Bind the last buffer we wrote to as our source for read operations
	lastPass->bind(RenderTargetBinding::Read);
	// Copies the image from lastPass into the default back buffer
	Framebuffer::blitBuffers({ 0, 0, lastPass->getWidth(), lastPass->getHeight() },
		{ 0, 0, app->GetWindow()->GetWidth(), app->GetWindow()->GetHeight() },
		BufferFlags::All, MagFilter::Nearest);

	// Unbind the last buffer from read operations, so we can write to it again later
	lastPass->unbind();
}

void PostProcessingLayer::Update() {
	auto it = _passes.find("highlight");
	if(it != _passes.end()) {
		if(bloom) {
			it->second->active = true;
			it->second->shader->SetUniform("uBloomThreshold", bloomThreshold);
		}
		else {
			it->second->active = false;
		}
	}

	for(unsigned i = 1; i <= 10; ++i) {
		it = _passes.find("hBlur" + std::to_string(i));
		if(it != _passes.end()) {
			if(bloom) {
				it->second->active = true;
				it->second->shader->SetUniform("isHorizontal", 1);
			}
			else {
				it->second->active = false;
			}
		}

		it = _passes.find("vBlur" + std::to_string(i));
		if(it != _passes.end()) {
			if(bloom) {
				it->second->active = true;
				it->second->shader->SetUniform("isHorizontal", 0);
			}
			else {
				it->second->active = false;
			}
		}
	}

	it = _passes.find("additiveBlend");
	if(it != _passes.end()) {
		if(bloom) {
			it->second->active = true;
		}
		else {
			it->second->active = false;
		}
	}

	it = _passes.find("motionBlur");
	if(it != _passes.end()) {
		if(motionBlur) {
			it->second->active = true;
		}
		else {
			it->second->active = false;
		}
	}
	
	it = _passes.find("checkerboard");
	if(it != _passes.end()) {
		if(checkerboard) {
			it->second->active = true;
			it->second->shader->SetUniform("xCheckerSize", checkerSize);
			it->second->shader->SetUniform("xCheckerColor", checkerColour);
		}
		else {
			it->second->active = false;
		}
	}
	
	it = _passes.find("inversion");
	if(it != _passes.end()) {
		if(inversion) {
			it->second->active = true;
		}
		else {
			it->second->active = false;
		}
	}
	
	it = _passes.find("grayscale");
	if(it != _passes.end()) {
		if(grayscale) {
			it->second->active = true;
		}
		else {
			it->second->active = false;
		}
	}
	
	it = _passes.find("kernel");
	if(it != _passes.end()) {
		if(kernel) {
			it->second->active = true;

			if(!blur && !sharpen && !edge) {
				blur = true;
			}

			// Blur
			const std::vector<float> blurKernel = {
				1.0f / 16, 2.0f / 16, 1.0f / 16,
				2.0f / 16, 4.0f / 16, 2.0f / 16,
				1.0f / 16, 2.0f / 16, 1.0f / 16
			};

			// Sharpen
			const std::vector<float> sharpenKernel = {
				-1.0f, -1.0f, -1.0f,
				-1.0f,  9.0f, -1.0f,
				-1.0f, -1.0f, -1.0f
			};

			// Edge sharpening
			const std::vector<float> edgeKernel = {
				1.0f,  1.0f, 1.0f,
				1.0f, -8.0f, 1.0f,
				1.0f,  1.0f, 1.0f
			};

			if(blur) {
				it->second->shader->SetUniforms<float>("uKernel", static_cast<int>(blurKernel.size()), blurKernel.data());
			}
			else if(sharpen) {
				it->second->shader->SetUniforms<float>("uKernel", static_cast<int>(sharpenKernel.size()), sharpenKernel.data());
			}
			else if(edge) {
				it->second->shader->SetUniforms<float>("uKernel", static_cast<int>(edgeKernel.size()), edgeKernel.data());
			}
		}
		else {
			it->second->active = false;
		}
	}

	it = _passes.find("gamma");
	if(it != _passes.end()) {
		if(gamma) {
			it->second->active = true;
			it->second->shader->SetUniform("uGamma", gammaValue);
		}
		else {
			it->second->active = false;
		}
	}
}

void PostProcessingLayer::RenderGUI() {
	ImGui::Begin("Post processing effects");

	ImGui::Checkbox("Bloom", &bloom);
	if(bloom) {
		ImGui::DragFloat("Bloom threshold", &bloomThreshold, 0.01f, 0.01f, 1.0f);
	}

	ImGui::Checkbox("Motion blur", &motionBlur);
	
	ImGui::Checkbox("Checkerboard", &checkerboard);
	if(checkerboard) {
		ImGui::DragFloat("Checker size", &checkerSize);
		ImGui::ColorPicker3("Checker size", glm::value_ptr(checkerColour));
		ImGui::NewLine();
	}
	
	ImGui::Checkbox("Inversion", &inversion);
	ImGui::Checkbox("Grayscale", &grayscale);

	ImGui::Checkbox("Gamma correction", &gamma);
	if(gamma) {
		ImGui::DragFloat("Gamma", &gammaValue, 0.01f, 0.01f, 10.0f);
		ImGui::NewLine();
	}
	
	ImGui::Checkbox("Kernel", &kernel);
	if(kernel) {
		if(ImGui::RadioButton("Blur", blur)) {
			if(!kernel) kernel = true;
			blur = true;
			sharpen = false;
			edge = false;
		}

		if(ImGui::RadioButton("Sharpen", sharpen)) {
			if(!kernel) kernel = true;
			blur = false;
			sharpen = true;
			edge = false;
		}

		if(ImGui::RadioButton("Edge sharpen", edge)) {
			if(!kernel) kernel = true;
			blur = false;
			sharpen = false;
			edge = true;
		}
	}

	ImGui::End();
}

