#include "Framebuffer.h"

#include <Logging.h>
#include <glad/glad.h>
#include <florp/graphics/Texture2D.h>

// -----------------------------------------------------------------
// ----- Frame buffer ----------------------------------------------
// -----------------------------------------------------------------


Framebuffer::Framebuffer(const glm::ivec2& size, const uint8_t numSamples) :
	_size(size), _isValid(false), _binding(RenderTargetBinding::Draw) {
	LOG_ASSERT(size.x > 0 && size.y > 0, "Width and height must be greater than 0!");

	int maxSamples = 0;
	glGetIntegerv(GL_MAX_SAMPLES, &maxSamples);
	_numSamples = glm::clamp(static_cast<int>(numSamples), 1, maxSamples);

	glCreateFramebuffers(1, &myRendererID);		// myRendererID is from florp::graphics::ITexture

	if(_numSamples > 1) {
		_unsampledFramebuffer = std::make_shared<Framebuffer>(size.x, size.y, 1);
	}

	_renderBuffers.reserve(10);
	_drawBuffers.reserve(7);
}

Framebuffer::Framebuffer(const uint32_t width, const uint32_t height, const uint8_t numSamples) :
	Framebuffer({ width, height }, numSamples) {}

Framebuffer::~Framebuffer() {
	LOG_INFO("Deleting frame buffer with ID {}", myRendererID);
	glDeleteFramebuffers(1, &myRendererID);
}

const glm::vec2& Framebuffer::getSize() const { return _size; }
uint32_t Framebuffer::getWidth() const { return _size.x; }
uint32_t Framebuffer::getHeight() const { return _size.y; }

void Framebuffer::resize(const glm::ivec2& size) {
	LOG_ASSERT(size.x > 0 && size.y > 0, "Width and height must be greater than 0!");

	if(size.x == _size.x && size.y == _size.y) {
		return;
	}

	_size = size;
	for(auto& buffer : _renderBuffers) {
		addAttachment(buffer.second.description);
	}

	const bool bufferStatus = validateBuffer();
	LOG_ASSERT(bufferStatus, "Framebuffer is invalid!");

	if(_numSamples > 1) {
		_unsampledFramebuffer->resize(size.x, size.y);
	}
	
}
void Framebuffer::resize(const uint32_t width, const uint32_t height) { resize({ width, height }); }

Framebuffer::sptr Framebuffer::clone() {
	auto result = std::make_shared<Framebuffer>(_size.x, _size.y, _numSamples);

	for(const auto& buffer : _renderBuffers) {
		result->addAttachment(buffer.second.description);
	}
	result->validateBuffer();
	return result;
}

void Framebuffer::addAttachment(const RenderBufferDescription& description) {
	const auto it = _renderBuffers.find(description.attachment);
	
	if(it != _renderBuffers.end()) {
		LOG_WARN("An attachment is already bound to this slot, removing existing target...");
		if(it->second.isRenderBuffer) {
			glDeleteRenderbuffers(1, &it->second.rendererId);
		}
		else {
			it->second.resource = nullptr;
		}
	}
	else if(description.attachment >= RenderTargetAttachment::Color0 && description.attachment <= RenderTargetAttachment::Color7) {
		_drawBuffers.push_back(description.attachment);
		glNamedFramebufferDrawBuffers(myRendererID, _drawBuffers.size(), reinterpret_cast<const GLenum*>(_drawBuffers.data()));
	}

	
	RenderBuffer& buffer = _renderBuffers[description.attachment];
	buffer.description = description;
	buffer.isRenderBuffer = !description.isShaderReadable;

	if(buffer.isRenderBuffer) {
		glCreateRenderbuffers(1, &buffer.rendererId);

		if(_numSamples > 1) {
			glNamedRenderbufferStorageMultisample(buffer.rendererId, _numSamples, static_cast<GLenum>(description.targetType), _size.x, _size.y);
		}
		else {
			glNamedRenderbufferStorage(buffer.rendererId, static_cast<GLenum>(description.targetType), _size.x, _size.y);
		}

		glNamedFramebufferRenderbuffer(myRendererID, static_cast<GLenum>(description.attachment), GL_RENDERBUFFER, buffer.rendererId);
	}
	else {
		florp::graphics::Texture2dDescription imageDescription = florp::graphics::Texture2dDescription();
		imageDescription.Width = _size.x;
		imageDescription.Height = _size.y;
		imageDescription.WrapS = imageDescription.WrapT = florp::graphics::WrapMode::ClampToEdge;
		imageDescription.MinFilter = florp::graphics::MinFilter::Linear;
		imageDescription.Format = static_cast<florp::graphics::InternalFormat>(description.targetType);
		imageDescription.NumSamples = _numSamples;

		const florp::graphics::Texture2D::Sptr texture = std::make_shared<florp::graphics::Texture2D>(imageDescription);
		buffer.resource = texture;
		buffer.rendererId = texture->GetRenderID();

		glNamedFramebufferTexture(myRendererID, static_cast<GLenum>(description.attachment), buffer.rendererId, 0);
		if(_numSamples > 1) {
			_unsampledFramebuffer->addAttachment(description);
		}
	}
	
	_isValid = false;
}
void Framebuffer::addAttachment(const bool isShaderReadable, const RenderTargetType targetType, const RenderTargetAttachment attachment) { addAttachment({ isShaderReadable, targetType, attachment }); }

florp::graphics::Texture2D::Sptr Framebuffer::getAttachment(const RenderTargetAttachment attachment) {
	const auto it = _renderBuffers.find(attachment);

	if(_numSamples > 1) {
		return _unsampledFramebuffer->getAttachment(attachment);
	}
	
	if(it == _renderBuffers.end()) {
		return nullptr;
	}
	if(it->second.isRenderBuffer) {
		return nullptr;
	}
	
	return std::dynamic_pointer_cast<florp::graphics::Texture2D>(_renderBuffers[attachment].resource);
}

bool Framebuffer::validateBuffer() {
	if(_numSamples > 1) {
		_unsampledFramebuffer->validateBuffer();
	}
	
	const unsigned result = glCheckNamedFramebufferStatus(myRendererID, GL_FRAMEBUFFER);
	if(result == GL_FRAMEBUFFER_COMPLETE) {
		_isValid = true;
		return true;
	}

	switch(result) {
		case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
			LOG_ERROR("Rendertarget failed to validate. One of the attachment points is framebuffer incomplete.");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
			LOG_ERROR("Rendertarget failed to validate. There are no attachments!");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
			LOG_ERROR("Rendertarget failed to validate. Draw buffer is incomplete.");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
			LOG_ERROR("Rendertarget failed to validate. Read buffer is incomplete.");
			break;
		case GL_FRAMEBUFFER_UNSUPPORTED:
			LOG_ERROR("Rendertarget failed to validate. Check the formats of the attached targets");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
			LOG_ERROR("Rendertarget failed to validate. Check the multisampling parameters on all attached targets");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
			LOG_ERROR("Rendertarget failed to validate for unknown reason!");
			break;
		case GL_FRAMEBUFFER_INCOMPLETE_VIEW_TARGETS_OVR:
			LOG_ERROR("Rendertarget failed to validate. Multiview targets issue!");
			break;
		default:
			LOG_ERROR("Rendertarget failed to validate for unknown reason!");
			break;
	}
	
	_isValid = false;
	return false;
}

void Framebuffer::bind(const RenderTargetBinding binding) const {
	_binding = binding;
	glBindFramebuffer(static_cast<GLenum>(binding), myRendererID);
}

void Framebuffer::unbind() const {
	if(_binding == RenderTargetBinding::None) {
		return;
	}

	if(_numSamples > 1) {
		glBindFramebuffer(GL_READ_FRAMEBUFFER, myRendererID);
		glBindFramebuffer(GL_DRAW_BUFFER, _unsampledFramebuffer->myRendererID);

		blitBuffers({ 0, 0, _size.x, _size.y },
			{ 0, 0, _size.x, _size.y },
			BufferFlags::All, florp::graphics::MagFilter::Nearest);

		for(const auto& buffers : _renderBuffers) {
			if(isColorAttachment(buffers.first)) {
				glReadBuffer(static_cast<GLenum>(buffers.first));
				glDrawBuffer(static_cast<GLenum>(buffers.first));
				blitBuffers({ 0, 0, _size.x, _size.y },
					{ 0, 0, _size.x, _size.y },
					BufferFlags::Color, florp::graphics::MagFilter::Linear);
			}
		}

		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	}
	
	glBindFramebuffer(static_cast<GLenum>(_binding), 0);
	_binding = RenderTargetBinding::None;
}

void Framebuffer::blitBuffers(const glm::ivec4& srcBounds, const glm::ivec4& dstBounds, BufferFlags flags, florp::graphics::MagFilter filterMode) {
	glBlitFramebuffer(srcBounds.x, srcBounds.x, srcBounds.z, srcBounds.w,
		dstBounds.x, dstBounds.y, dstBounds.z, dstBounds.w,
		static_cast<GLbitfield>(flags), static_cast<GLenum>(filterMode));
}

void Framebuffer::SetDebugName(const std::string& value) { myDebugName = value; }

void Framebuffer::Bind(const uint32_t slot) {
	getAttachment(RenderTargetAttachment::Color0)->Bind(slot);
}

void Framebuffer::Bind(const uint32_t slot, const RenderTargetAttachment attachment) {
	getAttachment(attachment)->Bind(slot);
}


// -----------------------------------------------------------------
// ----- Render buffer ---------------------------------------------
// -----------------------------------------------------------------

Framebuffer::RenderBuffer::RenderBuffer() :
	rendererId(0),
	isRenderBuffer(false),
	description(RenderBufferDescription()) {}
