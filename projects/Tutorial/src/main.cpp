#include <Logging.h>

#include <florp/app/Application.h>
#include <florp/game/BehaviourLayer.h>
#include <florp/game/ImGuiLayer.h>

#include "LightingLayer.h"
#include "PostProcessingLayer.h"
#include "RenderLayer.h"
#include "SceneBuilder.h"


int main() {

	auto game = new florp::app::Application();
	game->AddLayer<florp::game::ImGuiLayer>();
	game->AddLayer<florp::game::BehaviourLayer>();
	
	game->AddLayer<SceneBuilder>();
	game->AddLayer<LightingLayer>();
	game->AddLayer<PostProcessingLayer>();
	game->AddLayer<RenderLayer>();
	
	game->Run();
	delete game;

	return 0;
}

