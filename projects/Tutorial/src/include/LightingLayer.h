#pragma once

#include <florp/app/ApplicationLayer.h>
#include <florp/graphics/Mesh.h>
#include <florp/graphics/Shader.h>

#include "Framebuffer.h"

class LightingLayer : public florp::app::ApplicationLayer {
public:

	void OnWindowResize(uint32_t width, uint32_t height) override;
	void Initialize() override;
	void PreRender() override;
	void PostRender() override;
	void RenderGUI() override;

private:

	florp::graphics::Mesh::Sptr _fullscreenQuad;
	florp::graphics::Shader::Sptr _shadowMappingShader;
	florp::graphics::Shader::Sptr _maskedShader;
	florp::graphics::Shader::Sptr _shadowCompositeShader;
	florp::graphics::Shader::Sptr _finalCompositeShader;
	Framebuffer::sptr _accumulationBuffer;

	glm::vec3 _ambientLight = { 0.05f, 0.05f, 0.05f };

	void postProcessLights();
	void postProcessShadows();
};
