#pragma once

#include "Framebuffer.h"

#include <florp/app/ApplicationLayer.h>
#include <florp/graphics/Shader.h>
#include <florp/graphics/Mesh.h>

class PostProcessingLayer : public florp::app::ApplicationLayer {
public:
	
	PostProcessingLayer();
	void OnWindowResize(uint32_t width, uint32_t height) override;
	void PostRender() override;

	void Update() override;
	void RenderGUI() override;

protected:
	Framebuffer::sptr _mainFrameBuffer;
	florp::graphics::Mesh::Sptr _fullscreenQuad;

	struct PostPass {
		using sptr = std::shared_ptr<PostPass>;
		
		bool active = false;
		florp::graphics::Shader::Sptr shader;
		Framebuffer::sptr output;
		struct Input {
			sptr pass;
			RenderTargetAttachment attachment = RenderTargetAttachment::Color0;
			bool usePreviousFrame = false;
		};
		
		std::vector<Input> inputs;
		float resolutionMultiplier = 1.0f;
	};

	PostPass::sptr createPass(const std::string& name, std::string_view fragmentPath, float scale = 1.0f) const;
	std::unordered_map<std::string, PostPass::sptr> _passes;

	bool bloom = false;
	float bloomThreshold = 0.75f;

	bool motionBlur = false;
	
	bool checkerboard = false;
	float checkerSize = 2.0f; glm::vec3 checkerColour = { 0.0f, 0.0f, 0.0f };
	
	bool inversion = false;
	bool grayscale = false;

	bool kernel = false, blur = false, sharpen = false, edge = false;

	bool gamma = false; float gammaValue = 2.2f;
};
