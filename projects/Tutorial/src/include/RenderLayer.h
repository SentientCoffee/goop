#pragma once

#include "Framebuffer.h"

#include <florp/app/ApplicationLayer.h>

class RenderLayer : public florp::app::ApplicationLayer {
public:
	
	void OnSceneEnter() override;

	// Render will be where we actually perform our rendering
	void Render() override;

	void OnWindowResize(uint32_t width, uint32_t height) override;
};
