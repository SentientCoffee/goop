#pragma once
#include "Framebuffer.h"

struct ShadowLight {

	Framebuffer::sptr shadowBuffer;
	florp::graphics::Texture2D::Sptr mask;
	florp::graphics::Texture2D::Sptr projectorImage;
	float projectorImageIntensity;

	glm::mat4 projection;
	glm::vec3 color;
	float attenuation;
};
