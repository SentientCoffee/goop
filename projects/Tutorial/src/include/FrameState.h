#pragma once

#include "Framebuffer.h"
#include <GLM/glm.hpp>

struct FrameState {
	Framebuffer::sptr output;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;
	glm::mat4 viewProjection;
};

struct AppFrameState {
	FrameState current;
	FrameState last;
};