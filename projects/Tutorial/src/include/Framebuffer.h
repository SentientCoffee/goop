#pragma once

#include <florp/graphics/IGraphicsResource.h>
#include <florp/graphics/ITexture.h>
#include <florp/graphics/Texture2D.h>

#include <GLM/glm.hpp>

#include <unordered_map>

enum class RenderTargetAttachment : uint32_t {
	None         = 0,
	// From glad.h
	Color0              = 0x8CE0,		// GL_COLOR_ATTACHMENT0
	Color1              = 0x8CE1,		// GL_COLOR_ATTACHMENT1
	Color2              = 0x8CE2,		// GL_COLOR_ATTACHMENT2
	Color3              = 0x8CE3,		// GL_COLOR_ATTACHMENT3
	Color4              = 0x8CE4,		// GL_COLOR_ATTACHMENT4
	Color5              = 0x8CE5,		// GL_COLOR_ATTACHMENT5
	Color6              = 0x8CE6,		// GL_COLOR_ATTACHMENT6
	Color7              = 0x8CE7,		// GL_COLOR_ATTACHMENT7
	Depth               = 0x8D00,		// GL_DEPTH_ATTACHMENT
	Stencil             = 0x8D20,		// GL_STENCIL_ATTACHMENT
	DepthStencil        = 0x821A		// GL_DEPTH_STENCIL_ATTACHMENT
};

constexpr bool isColorAttachment(const RenderTargetAttachment attachment) {
	return attachment >= RenderTargetAttachment::Color0 && attachment <= RenderTargetAttachment::Color7;
}


enum class RenderTargetType : uint32_t {
	Default             = 0,
	// From glad.h
	Red8                = 0x8229,		// GL_R8
	Red16               = 0x822A,		// GL_R16
	Red16F              = 0x822D,		// GL_R16F
	
	RG8                 = 0x822B,		// GL_RG8
	RG16                = 0x822C,		// GL_RG16
	RG16F               = 0x822F,		// GL_RG16F
	
	RGB8                = 0x8051,		// GL_RGB8
	RGB10               = 0x8052,		// GL_RGB10
	RGB16               = 0x8054,		// GL_RGB16
	RGB16F              = 0x881B,		// GL_RGB16F
	
	RGBA8               = 0x8058,		// GL_RGBA8
	RGBA16              = 0x805B,		// GL_RGBA16
	RGBA16F             = 0x881A,		// GL_RGBA16F
	
	Depth16             = 0x81A5,		// GL_DEPTH_COMPONENT16
	Depth24             = 0x81A6,		// GL_DEPTH_COMPONENT24
	Depth32             = 0x81A7,		// GL_DEPTH_COMPONENT32
	Depth32F            = 0x8CAC,		// GL_DEPTH_COMPONENT32F
	
	Stencil4            = 0x8D47,		// GL_STENCIL_INDEX4
	Stencil8            = 0x8D48,		// GL_STENCIL_INDEX8
	Stencil16           = 0x8D49,		// GL_STENCIL_INDEX16
	
	Depth24Stencil8     = 0x88F0,		// GL_DEPTH24_STENCIL8
	Depth32fStencil8    = 0x8CAD		// GL_DEPTH32F_STENCIL8
};

enum class RenderTargetBinding : unsigned {
	None = 0,
	// From glad.h
	Draw                = 0x8CA9,		// GL_DRAW_FRAMEBUFFER
	Write               = 0x8CA9,		// GL_DRAW_FRAMEBUFFER
	Read                = 0x8CA8,		// GL_READ_FRAMEBUFFER
	ReadWrite           = 0x8D40		// GL_FRAMEBUFFER
};

enum class BufferFlags : unsigned {
	None                = 0,
	// From glad.h
	Color               = 0x00004000,		// GL_COLOR_BUFFER_BIT
	Depth               = 0x00000100,		// GL_DEPTH_BUFFER_BIT
	Stencil             = 0x00000400,		// GL_STENCIL_BUFFER_BIT
	All     = Color | Depth | Stencil		// GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT
	
};

struct RenderBufferDescription {
	bool isShaderReadable;
	RenderTargetType targetType;
	RenderTargetAttachment attachment;
};

class Framebuffer : public florp::graphics::ITexture {
public:
	
	using sptr = std::shared_ptr<Framebuffer>;

	Framebuffer(const glm::ivec2& size, uint8_t numSamples = 1);
	Framebuffer(uint32_t width, uint32_t height, uint8_t numSamples = 1);
	virtual ~Framebuffer();

	const glm::vec2& getSize() const;
	uint32_t getWidth() const;
	uint32_t getHeight() const;

	void resize(const glm::ivec2& size);
	void resize(uint32_t width, uint32_t height);

	sptr clone();

	void addAttachment(const RenderBufferDescription& description);
	void addAttachment(bool isShaderReadable, RenderTargetType targetType, RenderTargetAttachment attachment);
	florp::graphics::Texture2D::Sptr getAttachment(RenderTargetAttachment attachment);

	bool validateBuffer();

	void bind(RenderTargetBinding binding = RenderTargetBinding::Draw) const;
	void unbind() const;

	static void blitBuffers(const glm::ivec4& srcBounds, const glm::ivec4& dstBounds, BufferFlags flags = BufferFlags::All, florp::graphics::MagFilter filterMode = florp::graphics::MagFilter::Linear);
	
	void SetDebugName(const std::string& value) override;
	void Bind(uint32_t slot, RenderTargetAttachment attachment);
	void Bind(uint32_t slot) override;
	
protected:

	sptr _unsampledFramebuffer;
	
	glm::vec2 _size;
	uint8_t _numSamples;

	bool _isValid;

	mutable RenderTargetBinding _binding;

	struct RenderBuffer {
		unsigned rendererId;
		bool isRenderBuffer;
		IGraphicsResource::Sptr resource;
		RenderBufferDescription description;

		RenderBuffer();
	};

	std::unordered_map<RenderTargetAttachment, RenderBuffer> _renderBuffers;
	std::vector<RenderTargetAttachment> _drawBuffers;
	
};