#pragma once
#include "florp/game/IBehaviour.h"
#include <GLM/glm.hpp>

class ControlBehaviour : public florp::game::IBehaviour {
public:
	ControlBehaviour(const glm::vec3& speed) : IBehaviour(), _speed(speed), _yawPitch(glm::vec2(0.0f)) {}
	virtual ~ControlBehaviour() = default;

	void Update(entt::entity entity) override;

private:
	glm::vec3 _speed;
	glm::vec2 _yawPitch;
};


