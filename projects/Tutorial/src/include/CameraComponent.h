#pragma once

#include "Framebuffer.h"

#include <GLM/glm.hpp>

struct CameraComponent {
	bool isMainCamera = false;
	Framebuffer::sptr backBuffer;
	Framebuffer::sptr frontBuffer;
	glm::vec4 clearColor = glm::vec4(0.0f, 0.0f, 0.0f, 1);
	glm::mat4 projection;
};