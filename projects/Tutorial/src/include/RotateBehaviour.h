#pragma once

#include "florp/game/IBehaviour.h"
#include "florp/game/Transform.h"
#include "florp/game/SceneManager.h"
#include "florp/app/Timing.h"

#include <GLM/gtc/matrix_transform.hpp>

class RotateBehaviour : public florp::game::IBehaviour {
public:
	/**
	 * @brief Creates a new rotate behaviour with the given speed
	 * @param speed The speed to rotate along each axis, in degrees per second
	 */
	RotateBehaviour(const glm::vec3& speed) : IBehaviour(), _speed(speed) {}
	virtual ~RotateBehaviour() = default;

	void Update(const entt::entity entity) override {
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		transform.Rotate(_speed * florp::app::Timing::DeltaTime);
	}

private:
	glm::vec3 _speed;
};

class AxialSpinBehaviour : public florp::game::IBehaviour {
public:
	AxialSpinBehaviour(const glm::vec3& center, const glm::vec3& up, const float speed) : IBehaviour(), _startingPosition(0.0f), _center(center), _up(up), _angle(0.0f), _speed(speed) {}
	virtual ~AxialSpinBehaviour() = default;

	void OnLoad(const entt::entity entity) override {
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		_startingPosition = transform.GetLocalPosition();
	}
	
	void Update(const entt::entity entity) override {
		auto& transform = CurrentRegistry().get<florp::game::Transform>(entity);
		_angle += _speed * florp::app::Timing::DeltaTime;
		const glm::vec3 newPos = glm::translate(glm::mat4_cast(glm::angleAxis(glm::radians(_angle), _up)), _center) * glm::vec4(_startingPosition, 1.0f);
		transform.SetPosition(newPos);
	}

private:
	glm::vec3 _startingPosition;
	glm::vec3 _center;
	glm::vec3 _up;
	float _angle;
	float _speed;
};
