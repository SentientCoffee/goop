#version 450

layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;

layout (binding = 1) uniform sampler2D uGColor;
layout (binding = 2) uniform sampler2D uHDRAccumulation;

uniform float uExposure;

vec3 HDRToneMap(vec3 color, float exposure);

void main() {
	vec4 color = texture(uGColor, inUV) * texture(uHDRAccumulation, inUV);
	outColor = vec4(HDRToneMap(color.rgb, uExposure), 1.0);
}

vec3 HDRToneMap(vec3 color, float exposure) {
	const float gamma = 2.2; // Average gamma of most displays (NOTE: This can be a uniform, to allow for gamma controls)
	vec3 result = vec3(1.0) - exp(-color * exposure);
	result = pow(result, vec3(1.0 / gamma));

	return result;
}
