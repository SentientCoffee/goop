#version 450

layout(location = 0) in vec2 inUV;
layout(location = 1) in vec2 inScreenCoords;

layout(location = 0) out vec4 outColor;

uniform sampler2D xImage;
uniform mat4 uViewProjectionInverse;
uniform mat4 uLastViewProjection;

layout(binding = 1) uniform sampler2D uDepth;

const int numSamples = 5;

vec2 calculateMotion(sampler2D depth, mat4 viewProjectionInv, mat4 lastViewProjection);
vec4 calculateBlur(sampler2D tex, vec2 motion, vec2 uv);

void main() {
	vec2 motion = calculateMotion(uDepth, uViewProjectionInverse, uLastViewProjection);
	outColor = calculateBlur(xImage, motion, inUV);
}

vec2 calculateMotion(sampler2D depth, mat4 viewProjectionInv, mat4 lastViewProjection) {
	float zOverW = texture(depth, inUV).r;
	vec4 currentPosition = vec4(inUV.x * 2 - 1, (1 - inUV.y) * 2 - 1, zOverW, 1.0);
	vec4 d = currentPosition * viewProjectionInv;
	vec4 depthWorldPosition = d / d.w;

	vec4 previousPosition = depthWorldPosition * lastViewProjection;
	previousPosition /= previousPosition.w;
	return (currentPosition - previousPosition).xy / 2.0;
}

vec4 calculateBlur(sampler2D tex, vec2 motion, vec2 uv) {
	vec4 colour = vec4(0.0, 0.0, 0.0, 0.0);
	
	for(int i = 0; i < numSamples; ++i) {
		vec4 currentColour = texture(tex, uv);
		colour += currentColour;
		uv += motion;
	}

	vec4 finalColour = colour / numSamples;
	return finalColour;
}