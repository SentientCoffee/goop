#version 450

layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

uniform sampler2D xImage;
uniform float uGamma;

void main() {
	vec4 color = texture(xImage, inUV);
	outColor.rgb = pow(color.rgb, vec3(1.0/uGamma));
	outColor.a = color.a;
}