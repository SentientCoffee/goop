#version 450

layout (location = 0) in vec2 inUV;
layout (location = 1) in vec2 inScreenCoords;

layout (location = 0) out vec4 outColor;

layout (binding = 1) uniform sampler2D uCameraDepth;
layout (binding = 2) uniform sampler2D uShadowDepth;
layout (binding = 3) uniform sampler2D uGNormal;
layout (binding = 4) uniform sampler2D uProjection;

uniform mat4 uViewInverse;				// The inverse of the camera's view matrix (view->world)
uniform mat4 uProjectionInverse;		// The inverse of the camera's project matrix (clip->view)
uniform mat4 uViewProjectionInverse;	// The inverse of the camera's view-project matrix (clip->world)
uniform vec3 uCameraPos;				// The position of the camera, in world space

// The near and far clip planes for the camera
uniform float uNearPlane;
uniform float uFarPlane;

uniform mat4  uLightView;				// A matrix going from the world to the light space (world->light) basically the inverse of the light's transform
uniform vec3  uLightPos;				// The light's position, in world space
uniform vec3  uLightDirection;			// The light's direction, in world space
uniform vec3  uLightColor;				// The light's color
uniform float uLightAttenuation;		// The attenuation factor for the light (1/dist)
uniform float uBias = 0.01;				// The shadow biasing to use
uniform float uMaterialShininess;		// This should really be a GBuffer parameter

uniform bool  uIsProjector;				// Allows us to toggle between shadows and projectors
uniform float uProjectorIntensity;		// The intensity of the projector image

const vec3 HALF = vec3(0.5);
const vec3 DOUBLE = vec3(2.0);

vec3 unpackNormal(vec3 rawNormal);
vec4 getWorldPosition(vec2 uv);
vec3 calculateBlinnPhong(vec3 fragPos, vec3 fragNorm, vec3 lightPosition, vec3 lightColor, float lightAttenuation, float shadowFactor);
float calculatePCF(vec3 fragPos, float bias);

void main() {
	vec3 result = vec3(0.0);

	vec4 worldPosition = getWorldPosition(inUV);
	vec4 shadowPosition = uLightView * worldPosition;
	shadowPosition /= shadowPosition.w;
	shadowPosition = shadowPosition * 0.5 + 0.5;

	vec3 worldNormal = unpackNormal(texture(uGNormal, inUV).rgb);
	float bias = max(uBias * 10.0 * (1.0 - dot(worldNormal, uLightDirection)), uBias);

	// Determine our shadow factor using PCF
	float shadow = calculatePCF(shadowPosition.xyz, bias);

	if(shadowPosition.x < 0 || shadowPosition.x > 1 || shadowPosition.y < 0 || shadowPosition.y > 1 || shadowPosition.z < 0 || shadowPosition.z > 1) {
		shadow = 0.0;
	}

	if (uIsProjector) {
		// If the point we're trying to shade is outside of the lights clip region, we will simply discard and do no lighting
		if (shadowPosition.x < 0 || shadowPosition.x > 1 || shadowPosition.y < 0 || shadowPosition.y > 1 || shadowPosition.z < 0 || shadowPosition.z > 1) {
			discard;
		}
		// We can think of our projection texture as a filter over our light, so we can multiply them
		vec3 color = texture(uProjection, shadowPosition.xy).rgb * uLightColor;
		// We can do our blinn-phong model using the calculated light to be projected
		result = calculateBlinnPhong(worldPosition.xyz, worldNormal, uLightPos, color, uLightAttenuation, shadow);
	}
	else {
		// This is not a projector, just do the normal blinn-phong model using the lights color
		result = calculateBlinnPhong(worldPosition.xyz, worldNormal, uLightPos, uLightColor, uLightAttenuation, shadow);
	}
	
	// Output the result
	outColor = vec4(result, 1.0);
}

vec3 unpackNormal(vec3 rawNormal) {
	return (rawNormal - HALF) * DOUBLE;
}

// Calculates a world position from the main camera's depth buffer
vec4 getWorldPosition(vec2 uv) {
	// Get the depth buffer value at this pixel
	float zOverW = texture(uCameraDepth, uv).r * 2 - 1; // Convert depth from normal to clip space
	// Our clip space position (in the [-1,1] range)
	vec4 currentPos = vec4(uv.xy * 2 - 1, zOverW, 1); 
	// Transform by the view-projection inverse
	vec4 D = uViewProjectionInverse * currentPos;
	// Divide by w to get the world position
	vec4 worldPos = D / D.w;
	return worldPos;
}

vec3 calculateBlinnPhong(vec3 fragPos, vec3 fragNorm, vec3 lightPosition, vec3 lightColor, float lightAttenuation, float shadowFactor) {
	// Determine the direction from the position to the light
	vec3 toLight = lightPosition - fragPos;

	// Determine the distance to the light (used for attenuation later)
	float distToLight = length(toLight);
	// Normalize our toLight vector
	toLight = normalize(toLight);

	// Determine the direction between the camera and the pixel
	vec3 viewDir = normalize(uCameraPos - fragPos);

	// Calculate the halfway vector between the direction to the light and the direction to the eye
	vec3 halfDir = normalize(toLight + viewDir);

	// Our specular power is the angle between the the normal and the half vector, raised
	// to the power of the light's shininess
	float specPower = pow(max(dot(fragNorm, halfDir), 0.0), uMaterialShininess);

	// Finally, we can calculate the actual specular factor
	vec3 specOut = specPower * lightColor;

	// Calculate our diffuse factor, this is essentially the angle between
	// the surface and the light
	float diffuseFactor = max(dot(fragNorm, toLight), 0);
	// Calculate our diffuse output
	vec3  diffuseOut = diffuseFactor * lightColor;

	// We will use a modified form of distance squared attenuation, which will avoid divide
	// by zero errors and allow us to control the light's attenuation via a uniform
	float attenuation = 1.0 / (1.0 + lightAttenuation * pow(distToLight, 2));

	return (1.0 - shadowFactor) * attenuation * (diffuseOut + specOut);
}

float calculatePCF(vec3 fragPos, float bias) {
	float result = 0.0;
	vec2 texelSize = 1.0 / textureSize(uShadowDepth, 0); // Determine the texel size of the shadow sampler

	// Iterate over a 3x3 area of texels around our sample location
	for(int x = -1; x <= 1; ++x) { 
		for(int y = -1; y <= 1; ++y) {
			float pcfDepth = texture(uShadowDepth, fragPos.xy + vec2(x, y) * texelSize).r; // Sample the texture
			result += fragPos.z - bias > pcfDepth ? 1.0 : 0.0; // Perform the depth test, and add the result to the sum
		}    
	}
	result /= 9.0; // Average our sum
	return result;
}


