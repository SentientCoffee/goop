#version 450

layout(location = 0) in vec2 inUV;
layout(location = 1) in vec2 inScreenCoords;

layout(location = 0) out vec4 outColor;

uniform sampler2D xImage;
uniform float uKernel[10];

const float offset = 1.0 / 300.0;

void main() {
	// https://learnopengl.com/Advanced-OpenGL/Framebuffers for kernel implementation 

	vec2 offsets[9] = vec2[](
		vec2(-offset,   offset),	// top-left
		vec2( 0.0,      offset),	// top-center
		vec2( offset,   offset),	// top-right
		vec2(-offset,   0.0),   	// center-left
		vec2( 0.0,      0.0),   	// center-center
		vec2( offset,   0.0),   	// center-right
		vec2(-offset,  -offset),	// bottom-left
		vec2( 0.0,     -offset),	// bottom-center
		vec2( offset,  -offset) 	// bottom-right
	);

	vec3 sampleTextures[9];
	for(int i = 0; i < 9; i++) {
		sampleTextures[i] = texture(xImage, inUV.st + offsets[i]).rgb;
	}

	vec3 fragColor = vec3(0.0);
	for(int i = 0; i < 9; ++i) {
		fragColor += sampleTextures[i] * uKernel[i];
	}

	outColor = vec4(fragColor, 1.0);
}