#version 450

#define MAX_LIGHTS 50

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inWorldPos;
layout(location = 5) in vec2 inUV;

layout(location = 0) out vec4 outColor;
// Output normals to whatever is bound to the Color1 attachment
layout(location = 1) out vec3 outNormal;

uniform sampler2D uAlbedo;

uniform vec3  uCameraPos;

uniform vec3  uAmbientColor;
uniform float uAmbientPower;
uniform float uMaterialShininess;

struct Light {
	vec3 position;
	vec3 color;
	float attenuation;
};

uniform Light uLights[MAX_LIGHTS];
uniform int uNumLights;

vec3 resolvePointLight(Light light, vec3 norm);

void main() {
	// Our ambient is simply the color times the ambient power
	vec3 result = uAmbientColor * uAmbientPower;
	vec3 norm = normalize(inNormal); 

	for(int i = 0; i < uNumLights; ++i) {
		if(i >= MAX_LIGHTS) break;
		result += resolvePointLight(uLights[i], norm);
	}
	
	result = result * texture(uAlbedo, inUV).rgb * inColor.rgb;

	// Write the output
	outColor = vec4(result, inColor.a);
	outNormal = (norm / 2) + vec3(0.5); // Map from [0, 1] to [-1, 1]

}

vec3 resolvePointLight(Light light, vec3 norm) {
	vec3 toLight = light.position - inWorldPos;
	float distToLight = length(toLight);
	toLight = normalize(toLight);

	vec3 viewDir = normalize(uCameraPos - inWorldPos);
	vec3 halfDir = normalize(toLight + viewDir);

	float specularPower = pow(max(dot(norm, halfDir), 0.0), uMaterialShininess);
	vec3 specular = specularPower * light.color;

	float diffuseFactor = max(dot(norm, toLight), 0);
	vec3 diffuse = diffuseFactor * light.color;

	float attenuation = 1.0 / (1.0 + light.attenuation * pow(distToLight, 2));
	return attenuation * (diffuse + specular);
}
