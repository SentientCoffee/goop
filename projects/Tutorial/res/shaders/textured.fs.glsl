#version 450

layout(location = 5) in vec2 inUV;

layout(location = 0) out vec4 outColor;

uniform sampler2D uSampler;

void main() {
	// Write the output
	outColor = texture(uSampler, inUV);
}