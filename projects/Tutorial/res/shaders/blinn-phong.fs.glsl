#version 450

#define MAX_LIGHTS 50

layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inNormal;
layout(location = 2) in vec3 inWorldPos;

layout(location = 0) out vec4 outColor;

uniform vec3  uCameraPos;

uniform vec3  uAmbientColor;
uniform float uAmbientPower;

struct Light {
	vec3 position;
	vec3 color;
	float shininess;
	float attenuation;
};

uniform Light uLights[MAX_LIGHTS];
uniform int uNumLights;

void main() {
	// Our ambient is simply the color times the ambient power
	vec3 result = uAmbientColor * uAmbientPower;

	for(int i = 0; i < uNumLights; ++i) {
		if(i >= MAX_LIGHTS) break;

		// Re-normalize our input, so that it is always length 1
		vec3 norm = normalize(inNormal);
		// Determine the direction from the position to the light
		vec3 toLight = uLights[i].position - inWorldPos;
		// Determine the distance to the light (used for attenuation later)
		float distToLight = length(toLight);
		// Normalize our toLight vector
		toLight = normalize(toLight);

		// Determine the direction between the camera and the pixel
		vec3 viewDir = normalize(uCameraPos - inWorldPos);

		// Calculate the halfway vector between the direction to the light and the direction to the eye
		vec3 halfDir = normalize(toLight + viewDir);

		// Our specular power is the angle between the the normal and the half vector, raised
		// to the power of the light's shininess
		float specularPower = pow(max(dot(norm, halfDir), 0.0), uLights[i].shininess);

		// Finally, we can calculate the actual specular factor
		vec3 specular = specularPower * uLights[i].color;

		// Calculate our diffuse factor, this is essentially the angle between
		// the surface and the light
		float diffuseFactor = max(dot(norm, toLight), 0);
		// Calculate our diffuse output
		vec3 diffuse = diffuseFactor * uLights[i].color;

		// We will use a modified form of distance squared attenuation, which will avoid divide
		// by zero errors and allow us to control the light's attenuation via a uniform
		float attenuation = 1.0 / (1.0 + uLights[i].attenuation * distToLight);

		// Our result is our lighting multiplied by our object's color
		result += (attenuation * (diffuse + specular)) * inColor.xyz;

		// TODO: gamma correction
	}
	
	// Write the output
	outColor = vec4(result, inColor.a);// * a_ColorMultiplier;
}