#version 450

layout (location = 0) in vec3 inPosition;
layout (location = 1) in vec4 inColor;
layout (location = 2) in vec3 inNormal;
layout (location = 5) in vec2 inUV;

layout (location = 0) out vec4 outColor;
layout (location = 1) out vec3 outNormal;
layout (location = 2) out vec3 outWorldPos;
layout (location = 5) out vec2 outUV;

uniform mat4 uViewProjection;
uniform mat4 uModel;
uniform mat3 uNormalMatrix;

void main() {
	outColor = inColor;
	outNormal = uNormalMatrix * inNormal;
	outWorldPos =  (uModel * vec4(inPosition, 1)).xyz;
	outUV = inUV;

	gl_Position = uViewProjection * uModel * vec4(inPosition, 1);
}