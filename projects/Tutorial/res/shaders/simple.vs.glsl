#version 450

layout (location = 0) in vec3 inPosition;

uniform mat4 uModelViewProjection;

void main() {
	gl_Position = uModelViewProjection * vec4(inPosition, 1.0);
}