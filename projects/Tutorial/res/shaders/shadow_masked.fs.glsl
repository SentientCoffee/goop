#version 450

layout (binding = 0) uniform sampler2D uMask;
uniform vec2 uOutputResolution;

out float gl_FragDepth;

void main() {
	if (texture(uMask, gl_FragCoord.xy / uOutputResolution).r < 0.5f)
		gl_FragDepth = 0.0f;
	else
		gl_FragDepth = gl_FragCoord.z;
}
